<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Post;
use App\Category;
use \Auth;



class HomeController extends Controller
{
    public function index()
    {
    	$posts = Post::paginate(6);
        $users = Auth::user();

    	return view('pages.index', ['posts' => $posts ,'users' => $users] );
    }


    public function search()
    {
        $users = Auth::user();

        $searchkey = \Request::get('title');
        if($searchkey != ""){
            $posts = Post::where('title', 'like', '%' .$searchkey. '%')->orderBy('id')->paginate(6);
            if(count($posts) > 0 ){
                return view('pages.search',['posts' => $posts,'users' => $users]);
            }
        }
        else{
            return redirect()->back()->with('status','по такому запросу ничего не найдено!');
        }


    }




    public function show($slug)
    {
    	$post = Post::where('slug', $slug)->firstOrFail();
        $users = Auth::user();

        event('postHasViewed', $post);
    	return view('pages.show', ['post' => $post, 'users' => $users]);
    }

    public function tag($slug)
    {
        $tag = Tag::where('slug', $slug)->firstOrFail();
        $users = Auth::user();

        $posts = $tag->posts()->paginate(2);

        return view('pages.list', ['posts'  =>  $posts, 'users' => $users ]);
    }

    public function category($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $users = Auth::user();

        $posts = $category->posts()->paginate(4);

        return view('pages.list', ['posts'  =>  $posts, 'users' => $users ]);
    }
}
