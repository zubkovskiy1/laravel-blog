<nav class="navbar main-menu navbar-default">
    <div class="container">
        <div class="menu-content">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/images/logo.png" alt=""></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                {{--<ul class="nav navbar-nav text-uppercase">--}}

                {{--</ul>--}}

                <div class="form-wrapp">
                    <form class="navbar-form" role="search" method="get" action="{{url("/search")}}">
                        {{--<div class="input-grup search">--}}
                            <input type="text" class="form-control mr-sm-2 " placeholder="search" name="title">
                            <div class="input-group-btn custom">
                                <button class="btn btn-secondary my-2 my-sm-0 " type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        {{--</div>--}}
                    </form>
                </div>
                <ul class="nav navbar-nav text-uppercase pull-right">
                    @if(Auth::check())
                        <li>
                            <a href="/profile">My profile <img src="{{$users->getImage()}}" alt="" class="profile-img"></a>
                        </li>
                    @else
                        <li><a href="/register">Register</a></li>
                        <li><a href="/login">Login</a></li>
                    @endif

                </ul>

            </div>



            <div class="show-search">
                <form role="search" method="get" id="searchform" action="#">
                    <div>
                        <input type="text" placeholder="Search and hit enter..." name="s" id="s">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</nav>