 <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="treeview">
      <a href="{{route('admin')}}" class="{{( Request::is('admin') ? ' active' : null)}}">
        <i class="fa fa-dashboard"></i> <span>Админ-панель</span>
      </a>
    </li>
    <li>
        <a href="{{route('posts.index')}}" class="{{(url()->current()==url('/admin/posts/') ? ' active ' : null)}}
                                                  {{(url()->current()==url('/admin/posts/create') ? ' active ' : null)}}
                                                  {{(url()->current()==url('/admin/posts/{id)/edit') ? ' active ' : null)}}">
            <i class="fa fa-sticky-note-o"></i>
            <span>Посты</span>
        </a>
    </li>
    <li>
        <a href="{{route('categories.index')}}" class="{{(url()->current()==url('/admin/categories/') ? ' active ' : null)}}
                                                       {{(url()->current()==url('/admin/categories/create') ? ' active ' : null)}}">
            <i class="fa fa-list-ul"></i>
            <span>Категории</span>
        </a>
    </li>
    <li>
        <a href="{{route('tags.index')}}" class="{{(url()->current()==url('/admin/tags/') ? ' active ' : null)}}
                                                 {{(url()->current()==url('/admin/tags/create') ? ' active ' : null)}}">
            <i class="fa fa-tags"></i>
            <span>Теги</span>
        </a>
    </li>
    <li>
      <a href="{{route('comments')}}" class="{{(url()->current()==url('/admin/comments/') ? ' active ' : null)}}">
        <i class="fa fa-commenting"></i> <span>Комментарии</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-green">{{$newCommentsCount}}</small>
        </span>
      </a>
    </li>
    <li>
        <a href="{{route('users.index')}}" class="{{(url()->current()==url('/admin/users/') ? ' active ' : null)}}
                                                  {{(url()->current()==url('/admin/users/create') ? ' active ' : null)}}">
            <i class="fa fa-users"></i>
            <span>Пользователи</span>
        </a>
    </li>
    <li>
        <a href="{{route('subscribers.index')}}" class="{{(url()->current()==url('/admin/subscribers/') ? ' active ' : null)}}
                                                        {{(url()->current()==url('/admin/subscribers/create') ? ' active ' : null)}}">
            <i class="fa fa-user-plus"></i>
            <span>Подписчики</span>
        </a>
    </li>

</ul>